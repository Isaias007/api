//Llamamos a express para usar su enrutador
let express = require('express');
let router = express.Router();

//Llamamos al controlador para que cada endpoint haga una funcion del mismo
let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");


// El endpoint "/" realizara el get de las tareas
router.get("/", usuariosControllerAPI.usuario_list);
// El endpoint "/create" realizara el post de creacion de las tareas
router.post("/create", usuariosControllerAPI.usuario_create);
// El endpoint "/delete" realizara el delete de las tareas 
router.delete("/delete", usuariosControllerAPI.usuario_delete);
// El enpoint "/update" realizara el put para las actualizaciones de la tarea indicada
router.put("/update", usuariosControllerAPI.usuario_update);






module.exports = router;