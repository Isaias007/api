//Llamamos a express para usar el enrutador
var express = require('express');
//Llamamos al controllador 
var authControllerAPI = require('../../controllers/api/authControllerAPI');

var router = express.Router();



//Aqui ponemos el endpoint para enviar la autenticacion del usuario
router.post('/authenticate', authControllerAPI.authenticate);



module.exports = router;