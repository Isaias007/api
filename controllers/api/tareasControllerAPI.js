//Aqui en el controlador llamamos al modelo de las tareas con un require y es donde crearemos cada funcion que realizara el mismo
let Tarea = require("../../models/Tareas");


// Esta funcion es para mostrar todas las tareas 
exports.tarea_list = function (req, res) {

    Tarea.allTask(function (err, tasks) {
        if (err) {
            res.status(500).send(err.message);
        }

        res.status(200).json({
            Tareas: tasks
        });

    });

};

// Esta funcion es para crear cada tarea
 exports.tarea_create = function (req, res) {


    let task = new Tarea({

        tareaID: req.body.tareaID,

        nombre: req.body.nombre,

        email: req.body.email,

        idioma: req.body.idioma,

        requerimiento: req.body.requerimientos,

        pago: req.body.pago

    });


    Tarea.add(task, function (err, newTask) {

        if (err) {
            res.status(500).send(err.message);
        }

        res.status(201).send(newTask);


    });


};

//Esta funcion es para eliminar cada tarea
exports.tarea_delete = function(req,res){

    let tareaID = req.body.tareaID;

    Tarea.removeById(tareaID, function(err, tareaID){
        if(err){
            res.status(500).send(err.message);
        };

        res.status(201).send(tareaID);
    });

};

//Esta funcion es para editarlas mediante el id de moongose
exports.tarea_update = function(req,res){

    let filtro = req.body._id;


   Tarea.updateById(filtro, req.body, function(err, task){
       if(err){
           res.status(500).send(err.message);
       };
       task = req.body;
       res.status(201).send(task);
   });
}