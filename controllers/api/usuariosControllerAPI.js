//Llamamos al modulo de usuario para usar los metodos que creamos en el 
let Usuario = require("../../models/Usuarios");


//Con esta funcion usamos el metodo AllUser para encontrar todos los usuarios
exports.usuario_list = function (req, res) {

    Usuario.allusers(function (err, usuario) {
        if (err) {
            res.status(500).send(err.message);
        }

        res.status(200).json({
            Usuario: usuario
        });

    });

};

//Con esta usamos el metodo add que nos ayudara a crear los usuarios
exports.usuario_create = function (req, res) {


    let user = new Usuario({

        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password

    });


    Usuario.add(user, function (err, newUser) {

        if (err) {
            res.status(500).send(err.message);
        }

        res.status(201).send(newUser);


    });
};

//Aqui usamos el metodo removeByID para borrar dichos usuarios
exports.usuario_delete = function (req, res) {

    let usuarioId = req.body._id;

    Usuario.removeById(usuarioId, function (err, usuarioId) {
        if (err) {
            res.status(500).send(err.message);
        };

        res.status(201).send(usuarioId);
    });

};

//Y la funcion update que usaremos el metodo updateById para editarlos
exports.usuario_update = function (req, res) {

    let filtro = req.body._id;

    Usuario.updateById(filtro, req.body, function (err, usuario) {
        if (err) {
            res.status(500).send(err.message);
        };
        usuario = req.body;
        res.status(201).send(usuario);
    });
}