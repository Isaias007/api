//Esto son los requires de los middelwares , express etc..
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var jwt = require("jsonwebtoken");



//Conexion a mongo y la base de datos que usaremos 
mongoose.connect('mongodb://192.168.31.5/api_tareas', { useUnifiedTopology: true, useNewUrlParser: true });

mongoose.Promise = global.Promise;

var db = mongoose.connection;

//darme errores si la conexion a mongo es erronea

db.on("error", console.error.bind('Error de conexión con MongoDB'));

//Aqui traemos las rutas de cada cosa con los requires 
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tareaAPIRouter = require('./routes/api/tareas');
var usuarioAPIRouter = require('./routes/api/usuarios');
var authAPIRouter = require('./routes/api/auth');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.set('secretkey', 'JWT_PWD_!!223344');


//Rutas que se usaran
app.use('/', indexRouter);
app.use('/users', usersRouter);
//Esta es la que usaremos en la api + los endpoints
                      //Aqui usariamos la funcion para validar usuarios que tenemos más abajo
app.use('/api/tareas',validarUsuario, tareaAPIRouter);
app.use('/api/usuarios', usuarioAPIRouter);
app.use('/api/auth', authAPIRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




//Con esta funcion validariamos si el token que pusimos en el header del postman es correcto
function validarUsuario(req, res, next) {

  jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function (err, decoded) {

    if (err) {

      res.json({ status: "error", message: err.message, data: null });

    } else {

      req.body.userId = decoded.id;

      console.log('jwt verify: ' + decoded);

      next();

    }

  });
}







module.exports = app;
