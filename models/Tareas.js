// En el modelo crearemos el esquema de mongoose que es donde se guardaran las tareas

//Aqui llamamos a mongoose y lo usamos para crear los objetos schema
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

// Creamos el schema de tarea que sera la que usemos para cada funcion que usaremos en el controlador 
let tareaSchema = new Schema({

    tareaID: Number,

    nombre: String,

    email: String,

    idioma: String,

    requerimiento: String,

    pago: Number

});


//El metodo para encontrar todas las tareas
tareaSchema.statics.allTask = function (cb) {
    return this.find({}, cb);

};


//El de añadir a el schema 
tareaSchema.statics.add = function(task, cb) {

    return this.create(task, cb);

};

//Metodo para boorar mediante el id de la tarea
tareaSchema.statics.removeById = function(taskID, cb){

    return this.remove({tareaID:taskID}, cb);

};

//Metodo para encontrar mediante el id de mongoose
tareaSchema.statics.findById = function (taskID, cb){

    console.log(taskID);
    return this.findOne({_id:taskID}, cb);


};

//Y el metodo de edicion mediante el id de mongoose pasando un filtro que sera dicho id y el update a realizar
tareaSchema.statics.updateById = function(filtro, update, cb){

    return this.findByIdAndUpdate(filtro, update, cb);

};















module.exports = mongoose.model ("Tarea", tareaSchema);