//Vamos requeriendo mongoose para los schemas y bycript para cifrar las contraseñas
let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let bcrypt = require("bcrypt");
let saltRounds = 10;


//Aqyu creanis esta funcion para validar el email del usuario
let validateEmail = function (email) {

    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    return re.test(email);

}

//Creamos el schema del mismo con su nombre , email y password
let usuarioSchema = new Schema({

    nombre: {

        type: String,

        trim: true,


    },

    email: {

        type: String,

        trim: true,

        lowercase: true,

        unique: true,

        validate: [validateEmail, "Por favor, introduzca un email válido"],

        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/]

    },

    password: {

        type: String,

    },

    passwordResetToken: String,

    passwordResetTokenExpires: Date,


});

//Un metodo pre que funcionara como un middelware para encriptar la contraseña
usuarioSchema.pre("save", function (next) {

    if (this.isModified("password")) {

        this.password = bcrypt.hashSync(this.password, saltRounds);

    }

    next();

});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Metodo para ver todos los usuarios
usuarioSchema.statics.allusers = function (cb) {
    return this.find({}, cb);

};


//Metodo para crear un usuario
usuarioSchema.statics.add = function (users, cb) {

    return this.create(users, cb);

};

//Metodo para borrar mediante el _id del usuario
usuarioSchema.statics.removeById = function (usersId, cb) {

    return this.remove({ _id: usersId }, cb);

};

//Metodo para encontrar el usuario mediante el _id 
usuarioSchema.statics.findById = function (usersId, cb) {

    console.log(usersId);
    return this.findOne({ _id: usersId }, cb);


};

//Metodo para hacer el update donde el "filtro" sera como encontraremos el usuario y "update" lo que vamos a modificar
usuarioSchema.statics.updateById = function (filtro, update, cb) {

    return this.findByIdAndUpdate(filtro, update, cb);

};


module.exports = mongoose.model("Usuarios", usuarioSchema);